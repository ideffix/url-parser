import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by IdeFFiX on 19.03.2017.
 */
public class UrlParser {

    public static List<String> getAllUrlsFromFile(File file) {
        Scanner scanner = null;
        List<String> urls = new ArrayList<String>();
        try {
            scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                urls.add(scanner.nextLine());
            }
            return urls;
        } catch (FileNotFoundException e) {
            System.err.println("File not found in " + file.getAbsolutePath());
            e.printStackTrace();
        } finally {
            scanner.close();
        }
        return urls;
    }

    public static List<String> getUniqueDomainLinks(List<String> fullUrls) {
        List<String> uniqueDomainLinks = new ArrayList<String>();
        for (String actualUrl : fullUrls) {
            try {
                URL url = new URL(actualUrl);
                String cleanUrl = url.getHost().replace("www.", "");
                if (uniqueDomainLinks.contains(cleanUrl)) {
                    continue;
                } else {
                    uniqueDomainLinks.add(cleanUrl);
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return uniqueDomainLinks;
    }
}
