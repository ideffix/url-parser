import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by IdeFFiX on 19.03.2017.
 */
public class Main {

    public static void main(String[] args) {
        List<String> uniqueForexForumLinks = UrlParser.getUniqueDomainLinks(UrlParser.getAllUrlsFromFile(new File("src/main/resources/forexfora.txt")));
        List<String> uniqueMagnetoForumLinks = UrlParser.getUniqueDomainLinks(UrlParser.getAllUrlsFromFile(new File("src/main/resources/magnesyfora.txt")));
        System.out.println("Lista unikalnych linków do for dla Forex: " + uniqueForexForumLinks.size());
        uniqueForexForumLinks.forEach(link -> System.out.println(link));
        System.out.println("\n------------------------- \n");
        System.out.println("Lista unikalnych linków do for dla Magnesów: " + uniqueMagnetoForumLinks.size());
        uniqueMagnetoForumLinks.forEach(link -> System.out.println(link));
        System.out.println("\n------------------------- \n");
        List<String> commonLinks = new ArrayList<String>(uniqueForexForumLinks);
        commonLinks.retainAll(uniqueMagnetoForumLinks);
        System.out.println("Ilość wspólnych for dla Forexa i Magnesów: " + commonLinks.size());
        commonLinks.forEach(link -> System.out.println(link));
        System.out.println("\n------------------------- \n");
        List<String> linksToForex = new ArrayList<String>(uniqueMagnetoForumLinks);
        linksToForex.removeAll(uniqueForexForumLinks);
        System.out.println("Ilość for które można dodać do forexa: " + linksToForex.size());
        linksToForex.forEach(link -> System.out.println(link));
        System.out.println("\n------------------------- \n");
        List<String> linksToMagneto = new ArrayList<String>(uniqueForexForumLinks);
        linksToMagneto.removeAll(uniqueMagnetoForumLinks);
        System.out.println("Ilość for które można dodać do magnesów: " + linksToMagneto.size());
        linksToMagneto.forEach(link -> System.out.println(link));
    }

}
